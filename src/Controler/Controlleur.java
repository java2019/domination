package Controler;

import Model.DominationConsole.Jeu;
import org.newdawn.slick.AppGameContainer;
import org.newdawn.slick.SlickException;

public class Controlleur {
    public static int nbJoueurs;
    private Jeu jeu;
    private AppGameContainer app;

    /**
     * Permet de faire le lien entre l'app et le jeu en console lui même
     *
     * @param app
     * @param jeu
     */
    public Controlleur(AppGameContainer app, Jeu jeu) {
        this.app = app;
        this.jeu = jeu;
    }

    /**
     * Methode static permettant de lancer le jeu en console pour le moment
     */
    public static void launchConsole() {
        System.out.println("Lancement du jeu en console");
        Jeu jeu = JeuSingleton.getJeu().domination;
        //Le nombre de joueurs et les joueurs ont déjà étés ajoutés par interaction avec la vue
        //Créé la liste des dominos pour la partie
        jeu.creationListeDominoPartie();
        //Détermine l'ordre de départ de manière aléatoire
        jeu.ordreDeDepart();
        //Le jeu s'arrête une fois qu'un joueur ne peux plus jouer
        while (jeu.isEncours())
            jeu.tour(jeu.ordreTour());
        jeu.finDePartie();

    }

    public void init() {
        try {

            this.app.start();


        } catch (SlickException e) {
            e.printStackTrace();
        }
    }


}
