package Controler;

import Model.DominationConsole.Jeu;

/**
 * Notre dernier espoir
 */
public class JeuSingleton {
    private static JeuSingleton jeu = null;
    public Jeu domination;
    private String name;
    /**
     * Crée le jeu
     */
    public JeuSingleton() {
        this.domination = new Jeu();
    }
    /**
     * Retourne le Singleton s'il existe ou le créée
     * @return Jeu domination
     */
    public static JeuSingleton getJeu() {
        if (jeu == null) {
            jeu = new JeuSingleton();
        }
        return jeu;
    }

}
