import Controler.Controlleur;
import Model.DominationConsole.Jeu;
import View.DominationStates.DominationApp;
import org.newdawn.slick.AppGameContainer;
import org.newdawn.slick.SlickException;

public class Main {


    public static void main(String[] args) {

        AppGameContainer appGC;
        Controlleur controlleur;
        Jeu domination = new Jeu();


        try {
            appGC = new AppGameContainer(new DominationApp("Domination 2k19"));
            appGC.setDisplayMode(1000, 600, false);
            appGC.setTargetFrameRate(60);
            controlleur = new Controlleur(appGC, domination);
            controlleur.init();


        } catch (SlickException e) {
            e.printStackTrace();

        }

    }
}


