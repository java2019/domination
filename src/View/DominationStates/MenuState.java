package View.DominationStates;


import Model.Entities.Bouton;
import Model.SlickComponents.ComposantAbstrait;
import org.newdawn.slick.*;
import org.newdawn.slick.state.BasicGameState;
import org.newdawn.slick.state.StateBasedGame;

import java.util.ArrayList;

/**
 * MenuState principal du jeu
 */
public class MenuState extends BasicGameState {

    public String mouse, imageInfo;
    private float x, y = 0;
    private GameContainer gameContainer;
    private static Input input;
    private ArrayList<ComposantAbstrait> composants = new ArrayList<>();

    private Image selection, background;
    private boolean montrerInfosGUI;


    public MenuState(int state) {

    }

    @Override
    public int getID() {
        return 0;
    }

    /**
     * Charge les différentes ressources pour le menu
     *
     * @param gameContainer
     * @param stateBasedGame
     * @throws SlickException
     */
    @Override
    public void init(GameContainer gameContainer, StateBasedGame stateBasedGame) throws SlickException {
        this.gameContainer = gameContainer;
        gameContainer.setShowFPS(true);
        input = gameContainer.getInput();
        //change le souris: gameContainer.setMouseCursor(new Image("public/custom-mouse-m.png"),0,0);
        background = new Image("public/bg-menu.png");
        selection = new Image("public/Boutons/select.png");

        //Chargement des boutons
        this.composants.add(new Bouton(new Image("public/Boutons/bouton-jouer.png"), 340, 160, input, "jouer"));
        this.composants.add(new Bouton(new Image("public/Boutons/bouton-quitter.png"), 340, 300, input, "quitter"));

    }

    /**
     * Affiche les ressources
     *
     * @param gameContainer
     * @param stateBasedGame
     * @param graphics
     * @throws SlickException
     */
    @Override
    public void render(GameContainer gameContainer, StateBasedGame stateBasedGame, Graphics graphics) throws SlickException {
        graphics.drawImage(background, 0, 0);

        /**
         * Render tous les composants en forme de pile (boucle for pour rendre le code plus propre) et la forme derrière eux si la souris est par desssus
         */
        for (ComposantAbstrait composant : this.composants) {
            if (composant.isOn())
                graphics.drawImage(selection, composant.getPosX() - 20, composant.getPosY() - 30); //select
            composant.render(graphics);
        }


        /*
        affichage autre
         */
        if (montrerInfosGUI) infoGUI(graphics);




    }

    //EXECUTE DES ACTIONS SI TEL OU TEL BOUTTON EST APPUYE
    @Override
    public void update(GameContainer gameContainer, StateBasedGame stateBasedGame, int i) throws SlickException {

        /**
         * vérifie si un des composant a été cliqué et lui fait exécuter une action particulière
         */
        for (ComposantAbstrait composant : this.composants) { //parcours tous les composants à l'écran
            if (composant.isClicked()) { //détecte s'ils  ont été cliqué
                switch (composant.getNom()) { //en fonction du nom donné au composant assigne le nombre de joueurs au stateBasedGame
                    case "jouer":
                        stateBasedGame.enterState(3); //va au choix des joueurs
                        break;
                    case "quitter":
                        System.exit(0); //quitte le jeu
                        break;
                }
            }
        }



        //active ou desactive les informations sur le GUI
        if (input.isKeyPressed(Input.KEY_TAB)) {
            montrerInfosGUI = !montrerInfosGUI;
        }
    }

    /**
     * Affiche ou enlève (grâce à TAB) des informations sur le positionnement de la souris
     *
     * @param graphics
     */
    private void infoGUI(Graphics graphics) {
        graphics.drawString("Mouse  x: " + input.getMouseX() + " y: " + input.getMouseY(), 780, 10);
        graphics.drawString("TAB - cacher infosGUI", 780, 120);


    }
}


