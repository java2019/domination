package View.DominationStates;


import Model.Entities.Bouton;
import Controler.JeuSingleton;


import Model.SlickComponents.ComposantAbstrait;
import org.newdawn.slick.*;
import org.newdawn.slick.state.BasicGameState;
import org.newdawn.slick.state.StateBasedGame;

import java.util.ArrayList;


/**
 * MenuState principal du jeu
 */
public class ChoixJoueurState extends BasicGameState {

    private static Input input;
    private float x, y = 0;
    private GameContainer gameContainer;
    private int nombreJoueurs;
    private ArrayList<ComposantAbstrait> composants = new ArrayList<>();

    private Image background, selection;
    private boolean montrerInfosGUI;


    public ChoixJoueurState(int state) {

    }

    @Override
    public int getID() {
        return 3;
    }

    @Override
    public void init(GameContainer gameContainer, StateBasedGame stateBasedGame) throws SlickException {
        this.gameContainer = gameContainer;

        gameContainer.setShowFPS(true);
        input = gameContainer.getInput();

        background = new Image("public/bg-menu.png");
        selection = new Image("public/Boutons/select.png");

        //Chargement des boutons
        this.composants.add(new Bouton(new Image("public/Boutons/bouton-ia.png"), 180, 250, input, "ia"));
        this.composants.add(new Bouton(new Image("public/Boutons/bouton-2j.png"), 540, 250, input, "j2"));
        this.composants.add(new Bouton(new Image("public/Boutons/bouton-3j.png"), 180, 400, input, "j3"));
        this.composants.add(new Bouton(new Image("public/Boutons/bouton-4j.png"), 540, 400, input, "j4"));


    }

    @Override
    public void render(GameContainer gameContainer, StateBasedGame stateBasedGame, Graphics graphics) throws SlickException {
        graphics.drawImage(background, 0, 0);

        /**
         * Render tous les composants en forme de pile (boucle for pour rendre le code plus propre) et la forme derrière eux si la souris est par desssus
         */
        for (ComposantAbstrait composant : this.composants) {
            if (composant.isOn())
                graphics.drawImage(selection, composant.getPosX() - 20, composant.getPosY() - 30); //select
            composant.render(graphics);
        }



        /*
        affichage autre
         */
        if (montrerInfosGUI) infoGUI(graphics);
    }

    @Override
    public void update(GameContainer gameContainer, StateBasedGame stateBasedGame, int i) throws SlickException {
        /**
         * Assigne le nombre de joueurs et va au menu principal
         */


        for (ComposantAbstrait composant : this.composants) { //parcours tous les composants à l'écran
            if (composant.isClicked()) { //détecte s'ils  ont été cliqué
                switch (composant.getNom()) { //en fonction du nom donné au composant assigne le nombre de joueurs au stateBasedGame
                    case "ia":
                        this.nombreJoueurs = 1;
                        break;
                    case "j2":
                        this.nombreJoueurs = 2;
                        break;
                    case "j3":
                        this.nombreJoueurs = 3;
                        break;
                    case "j4":
                        this.nombreJoueurs = 4;
                        break;
                }
                System.out.println("nb joueurs :" + nombreJoueurs);
                JeuSingleton.getJeu().domination.setNombreDeJoueurs(nombreJoueurs);
                //gameContainer.sleep(50);
                if (nombreJoueurs == 1) {
                    System.out.println("désolé l'ia n'a pas été encore implémentée"); //va directement à l'état de jeu
                } else {
                    stateBasedGame.enterState(4);
                }
            }
        }


        if (input.isKeyDown(Input.KEY_M)) {
            stateBasedGame.enterState(0);
        }

        //active ou desactive les informations sur le GUI
        if (input.isKeyPressed(Input.KEY_TAB)) {
            montrerInfosGUI = !montrerInfosGUI;
        }
    }


    /**
     * Affiche ou enlève (grâce à TAB) des informations sur le positionnement de la souris
     *
     * @param graphics
     */
    private void infoGUI(Graphics graphics) {
        graphics.drawString("Mouse  x: " + input.getMouseX() + " y: " + input.getMouseY(), 780, 10);
        graphics.drawString("M - menu principal", 780, 100);
        graphics.drawString("TAB - cacher infosGUI", 780, 120);
    }


}
