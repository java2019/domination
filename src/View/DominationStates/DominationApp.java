package View.DominationStates;

import org.newdawn.slick.GameContainer;
import org.newdawn.slick.SlickException;
import org.newdawn.slick.state.StateBasedGame;

public class DominationApp extends StateBasedGame {

    //ETATS DIFFERENTS
    public static final int menuPrincipal = 0;
    public static final int jeu = 1;
    public static final int choixjoueurs = 3;
    public static final int choixpseudos = 4;


    //PROPRIETES APPLICATION
    public static final String nomJeu = "Domination 2k19";

    //CONSTRUCTEUR
    public DominationApp(String nomJeu) {
        super(nomJeu);
        this.addState(new MenuState(menuPrincipal));
        this.addState(new ChoixJoueurState(choixjoueurs));
        this.addState((new JeuState(jeu)));
        this.addState(new ChoixPseudoState(choixpseudos));

        this.enterState(menuPrincipal);

    }

    public static int getJeu() {
        return jeu;
    }

    //DIFFERENTS ETATS DU JEU (MENU PRINCIPAL - JEU ETC.)
    @Override
    public void initStatesList(GameContainer gameContainer) throws SlickException {

        this.getState(menuPrincipal).init(gameContainer, this);
        this.getState(choixjoueurs).init(gameContainer, this);
        this.getState(choixpseudos).init(gameContainer, this);
        this.getState(jeu).init(gameContainer, this);


    }



}
