package View.DominationStates;


import Model.SlickComponents.ComposantAbstrait;
import Controler.JeuSingleton;
import org.newdawn.slick.*;
import org.newdawn.slick.state.BasicGameState;
import org.newdawn.slick.state.StateBasedGame;

import java.util.ArrayList;
import java.util.Scanner;

/**
 * MenuState principal du jeu
 */
public class ChoixPseudoState extends BasicGameState {

    private static Input input;
    private String mouse, imageInfo;
    private float x, y = 0;
    private GameContainer gameContainer;
    private StateBasedGame stateBasedGame;
    //private int nombreJoueurs = JeuSingleton.getJeu().domination.getNombreDeJoueurs();
    private ArrayList<ComposantAbstrait> composants = new ArrayList<>();
    private Image background, selection;
    private boolean montrerInfosGUI;
    //private Jeu domination = JeuSingleton.getJeu().domination;
    private Scanner sc = new Scanner(System.in);
    private ArrayList<String> names = new ArrayList<>();

    public ChoixPseudoState(int state) {

    }

    @Override
    public int getID() {
        return 4;
    }

    @Override
    public void init(GameContainer gameContainer, StateBasedGame stateBasedGame) throws SlickException {
        this.gameContainer = gameContainer;

        gameContainer.setShowFPS(true);
        input = gameContainer.getInput();


        background = new Image("public/bg-choix.png");
        selection = new Image("public/Boutons/select.png");
        //Test de la saisie de text


    }

    @Override
    public void render(GameContainer gameContainer, StateBasedGame stateBasedGame, Graphics graphics) throws SlickException {

        graphics.drawImage(background, 0, 0);

        /*
        affichage autre
         */
        if (montrerInfosGUI) infoGUI(graphics);

        /**
         * affichage des noms stockés
         */
        if (names.isEmpty()) {
            graphics.drawString("Appuez sur ENTER pour insérer un pseudo dans la console : (" + JeuSingleton.getJeu().domination.getNombreDeJoueurs() + ")", 200, 280);
        } else {
            graphics.drawString("ENTER pour insérer un autre joueur: (" + (JeuSingleton.getJeu().domination.getNombreDeJoueurs() - names.size()) + ")", 200, 280);
            for (int i = 0; i < names.size(); i++) {
                graphics.drawString(i + 1 + " - " + names.get(i), 200, 300 + i * 20);
            }
        }
    }

    @Override
    public void update(GameContainer gameContainer, StateBasedGame stateBasedGame, int i) throws SlickException {
        /**
         * Assigne le nombre de joueurs et va au menu principal
         */

        if (input.isKeyDown(Input.KEY_M)) {
            stateBasedGame.enterState(0);
            JeuSingleton.getJeu().domination.setNombreDeJoueurs(0);

        }


        //Permet de rentrer de nouveaux joueurs
        int nbJoueurs = JeuSingleton.getJeu().domination.getNombreDeJoueurs();

        if (input.isKeyDown(Input.KEY_ENTER)) {
            if (names.size() < nbJoueurs) {
                if (this.names.size() < nbJoueurs) {
                    System.out.println("Entrez un pseudo :  ");
                    names.add(sc.nextLine());
                    if (this.names.size() == nbJoueurs) {
                        stateBasedGame.enterState(1);
                        JeuSingleton.getJeu().domination.addJoueurs2(names); //donne l'arraylist au jeu qui se charge de créer des joueurs
                    }
                }
            }
        }


        //active ou desactive les informations sur le GUI
        if (input.isKeyPressed(Input.KEY_TAB)) {
            montrerInfosGUI = !montrerInfosGUI;
        }


        if (input.isKeyPressed(Input.KEY_P)) {
            this.gameContainer.setPaused(true);
            onPause();
        }

    }

    /**
     * Affiche ou enlève (grâce à TAB) des informations sur le positionnement de la souris
     *
     * @param graphics
     */
    private void infoGUI(Graphics graphics) {
        graphics.drawString("Mouse  x: " + input.getMouseX() + " y: " + input.getMouseY(), 780, 10);
        graphics.drawString("M - menu principal", 780, 100);
        graphics.drawString("TAB - cacher infosGUI", 780, 120);
        graphics.drawString("Nb. jours -" + JeuSingleton.getJeu().domination.getNombreDeJoueurs(), 780, 80);
    }


    //testDu mode Pause
    private void onPause() {
        String nonesense;
        while (gameContainer.isPaused()) {
            System.out.println("pause?");
            nonesense = sc.nextLine();
            if (nonesense.equals("unpause")) {
                gameContainer.setPaused(false);
            }
        }
    }


}
