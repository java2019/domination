package View.DominationStates;


import Controler.Controlleur;
import Model.Entities.PlateauAffichage;
import Model.SlickComponents.ComposantAbstrait;
import Model.Entities.PiocheAffichage;

import Model.Entities.StatusAffichage;
import org.newdawn.slick.*;
import org.newdawn.slick.state.BasicGameState;
import org.newdawn.slick.state.StateBasedGame;

/**
 * MenuState principal du jeu
 */
public class JeuState extends BasicGameState {


    private float x, y = 0;
    private int inputX, inputY;
    private GameContainer gameContainer;
    private static Input input;
    private Image menuButton, fond, image;
    private PlateauAffichage plateau;
    private ComposantAbstrait pioche, status;
    private boolean montrerInfosGUI, randomInf;


    public JeuState(int state) {

    }

    @Override
    public int getID() {
        return 1;
    }

    @Override
    public void init(GameContainer gameContainer, StateBasedGame stateBasedGame) throws SlickException {
        this.gameContainer = gameContainer;
        gameContainer.setShowFPS(true);
        input = gameContainer.getInput();

        //Creation des composants principaux
        this.plateau = new PlateauAffichage(100, 25, new Image("public/map1.jpg"), input);
        this.pioche = new PiocheAffichage(new Image("public/domino-panel.png"), 700, 25, input);
        this.status = new StatusAffichage(new Image("public/status-panel.png"), 700, 355, input);


        this.montrerInfosGUI = true;
        this.randomInf = false;

        fond = new Image("public/bg-wood.png");
    }

    @Override
    public void render(GameContainer gameContainer, StateBasedGame stateBasedGame, Graphics graphics) throws SlickException {

        graphics.drawImage(fond, 0, 0);
        /**
         *
         * Affichage des composants
         *
         */
        //affiche à l'écran le plateau et pioche
        plateau.render(graphics);
        pioche.render(graphics);
        status.render(graphics);

        /*
        affichage autre
         */
        if (montrerInfosGUI) infoGUI(graphics);
    }

    @Override
    public void update(GameContainer gameContainer, StateBasedGame stateBasedGame, int i) throws SlickException {

        //update de l'input utilisateur utilisé tout au long du programe et sleep entre chaque update
        // gameContainer.sleep(30);
        this.inputX = input.getMouseX();
        this.inputY = input.getMouseY();

        /*
         * ******************************************
         *
         * Detection des inputs par rapport aux composants injectés
         *
         * **********************************************
         */

        //Si l'utilisateur est sur le plateau, ou la pioche ou status et detecter les sous composants

        plateau.isOn();
        pioche.isOn();
        status.isOn();


        /*
         * ******************************************
         *
         * Saisies clavier
         *
         * **********************************************
         */

        //lance le jeu en console
        if (input.isKeyPressed(Input.KEY_ENTER)) {
            Controlleur.launchConsole();
        }


        //appuyer sur L affiche le plateau en console
        if (input.isKeyPressed(Input.KEY_L)) {
            plateau.logPlateau();
        }

        //en appuyant sur R rajoute un domino random
        if (randomInf) plateau.placerRandom();

        if (input.isKeyPressed(Input.KEY_R)) {
            randomInf = !randomInf;

        }

        //en appuyant sur C rajoute un domino random
        if (input.isKeyPressed(Input.KEY_C)) {
            plateau.effacerGrille();
        }

        //active ou desactive les informations sur le GUI
        if (input.isKeyPressed(Input.KEY_TAB)) {
            montrerInfosGUI = !montrerInfosGUI;
        }


        //MenuState principal car voila
        if (input.isKeyPressed(Input.KEY_M)) {
            System.out.println("pressed MENU");
            stateBasedGame.enterState(0);
        }

        //Voir la pioche courante en appuyant sur P
        if (input.isKeyPressed(Input.KEY_P)) {
            System.out.println("PiocheAffichage Courante");
            pioche.voir();

        }

    }

    /**
     * Affiche ou enlève (grâce à TAB) des informations sur le positionnement de la souris
     *
     * @param graphics
     */
    private void infoGUI(Graphics graphics) {
        graphics.drawString("Mouse  x: " + input.getMouseX() + " y: " + input.getMouseY(), 780, 10);
        graphics.drawString("Image  x:" + x + " y: " + y, 780, 25);
        graphics.drawString("R - domino random", 780, 40);
        graphics.drawString("C - effacer dominos", 780, 60);
        graphics.drawString("L - log état grille", 780, 80);
        graphics.drawString("M - menu principal", 780, 100);
        graphics.drawString("TAB - cacher infosGUI", 780, 120);
    }
}
