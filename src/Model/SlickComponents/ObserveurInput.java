package Model.SlickComponents;

import org.newdawn.slick.Graphics;

/**
 * Interface Utilisee pour un Listener pattern afin d'écouter les clics
 */

public interface ObserveurInput {
    void detecteClic();

    void render(Graphics graphics);
}
