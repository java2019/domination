package Model.SlickComponents;

import org.newdawn.slick.Graphics;
import org.newdawn.slick.Image;
import org.newdawn.slick.Input;

import java.util.ArrayList;
import java.util.List;

/**
 *Definit ce qu'est un Composant
 */
public abstract class ComposantAbstrait implements Entitee {

    protected static Input input; //input de l'utilisateur
    protected Image imageFond; //image du fond du plateau
    protected int posX, posY; //position de placement du plateau dans la GUI
    protected String nom; //nom qui peut être donné au composant
    protected String isOnMessage = "COMPOSANT : " + this.getClass().toString(); //message par defaut quand le curseur
    protected String isClickedMessage = "cliked : " + this.getClass().toString(); //message quand on a clique
    protected List<ObserveurInput> observeurs = new ArrayList<>();
    protected String stringOut = ""; //message qui sera affiché au centre du composant s'il n'est pas vide
    protected int fontCoeff = 5;// estimation de la largueur d'un charactere affiché



    /**
     * Passe en entree l'image qui servira à illustrer le composant, la position x et position y de placement
     * ainsi que l'input d'utilisateur, l'instanciation se fait dans init
     *
     * @param imageFond
     * @param posX
     * @param posY
     * @param input
     */
    public ComposantAbstrait(Image imageFond, int posX, int posY, Input input) {
        this.imageFond = imageFond;
        this.posX = posX;
        this.posY = posY;
        this.input = input;
    }

    public static Input getInput() {
        return input;
    }

    @Override
    public void render(Graphics graphics) {
        graphics.drawImage(imageFond, posX, posY);
        for (ObserveurInput obs : observeurs) {
            obs.render(graphics);

        }

        if (!stringOut.equals("")) { //ne va render ce String s'il est vide
            int xCenter, yCenter; //position centre pour l'affichage

            xCenter = Math.round((float) posX + (float) imageFond.getWidth() / 2);
            yCenter = Math.round((float) posY + (float) imageFond.getHeight() / 2);
            graphics.drawString(stringOut, xCenter - stringOut.length() * fontCoeff, yCenter); //message par dessus le composant
        }

    }

    public Image getImageFond() {
        return imageFond;
    }

    public void setImageFond(Image imageFond) {
        this.imageFond = imageFond;
    }

    public int getPosX() {
        return posX;
    }

    public int getPosY() {
        return posY;
    }

    public String getNom() {
        return nom;
    }

    /**
     * Verifie que la souris est sur le composant
     *
     * @return bool
     */
    public boolean isOn() {
        if (input.getMouseX() > posX && input.getMouseX() < posX + imageFond.getWidth() && input.getMouseY() > posY && input.getMouseY() < posY + imageFond.getHeight()) {
            if (input.isMousePressed(1)) {
                System.out.println(isOnMessage); //indique que la souris est sur le composant dans console
            }
            for (ObserveurInput obs : observeurs) {
                obs.detecteClic();

            }
            return true;
        }
        return false;
    }

    //clic sur le boutton detection

    public boolean isClicked() {
        if (this.isOn() && input.isMouseButtonDown(0)) {
            System.out.println(isClickedMessage);
            return true;
        }
        return false;

    }

    public void voir() {
        System.out.println("Ceci est : " + this.getClass());
    }

    /**
     * Pattern Listener afin de savoir quand on clic sur les sous composants
     *
     * @param observeur
     */
    public void ajoutObservateur(ObserveurInput observeur) {
        this.observeurs.add(observeur);
    }


}
