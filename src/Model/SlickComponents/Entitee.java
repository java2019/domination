package Model.SlickComponents;


import org.newdawn.slick.Graphics;

public interface Entitee {

    /**
     * Utilisé afin de savoir si l'utilisateur se trouve au dessus de l'objet en lui même sur l'interface graphique
     * @return boolean
     */
    boolean isOn();

    /**
     * C'est ce qui va gerer l'affichage de l'objet en lui même
     * @param graphics
     */
    void render(Graphics graphics);
}
