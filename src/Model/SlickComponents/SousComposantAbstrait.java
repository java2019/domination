package Model.SlickComponents;

import org.newdawn.slick.Image;
import org.newdawn.slick.Input;

import org.newdawn.slick.Graphics;

/**
 *
 */
public abstract class SousComposantAbstrait implements Entitee, ObserveurInput {

    protected static Input input; //input de l'utilisateur
    protected Image imageFond; //image du fond du Sous composant
    protected ComposantAbstrait composant; //composant dans lequel il se trouve
    protected int posX, posY; //position de placement du plateau dans la GUI
    protected String isOnMessage = "curseur sur : " + this.getClass().toString();
    protected int pasAffichage, xOut, yOut;//message par defaut quand le cur
    protected String stringOut = ""; //vide de base


    /**
     * Un sous composant se trouve à une position particulière et à l'interieur d'un autre composant
     *
     * @param composant
     * @param posX
     * @param posY
     */
    public SousComposantAbstrait(int posX, int posY, ComposantAbstrait composant, Input input) {
        this.composant = composant;
        this.posX = posX;
        this.posY = posY;
        this.pasAffichage = 10;
        this.input = input;

        /**
         * Ajoute les sous-composants comme "à l'écoute" de leur composant
         */
        this.composant.ajoutObservateur(this);
    }

    /**
     * Verifie que la souris est sur le composant
     *
     * @return bool
     */
    public boolean isOn() {
        if (input.getMouseX() > posX && input.getMouseX() < posX + imageFond.getWidth() && input.getMouseY() > posY && input.getMouseY() < posY + imageFond.getHeight()) {
            if (input.isMousePressed(0)) {
                System.out.println(isOnMessage); //indique que la souris est sur le composant dans console
            }
            return true;
        }
        return false;
    }


    /**
     * Cette methode est exécutée par une ComposantAbstrait qui intègre ce composant en pattern observeur, detections des clics
     */
    @Override
    public void detecteClic() {
        if (input.getMouseX() > this.xOut && input.getMouseX() < this.xOut + imageFond.getWidth() && input.getMouseY() > this.yOut && input.getMouseY() < this.yOut + imageFond.getHeight()) {
            if (input.isMousePressed(0)) {
                System.out.println("Clic : " + this.isOnMessage);
            }
        }
    }


    @Override
    public void render(Graphics graphics) {
        int x = composant.getPosX();
        int y = composant.getPosY();

        //conversation facile avec posX et posY étant les coordonnées dans le tableau
        this.xOut = x + posX * pasAffichage;
        this.yOut = y + posY * pasAffichage;
        if (!stringOut.equals("")) { //ne va render ce String s'il est vide
            graphics.drawString(stringOut, xOut + pasAffichage, yOut + pasAffichage); //message par dessus le composant
        }
        graphics.drawImage(imageFond, xOut, yOut);

    }




    public Image getImageFond() {
        return imageFond;
    }

    public void setImageFond(Image imageFond) {
        this.imageFond = imageFond;
    }

    public int getPosX() {
        return posX;
    }

    public int getPosY() {
        return posY;
    }


    public void voir() {
        System.out.println("Ceci est : " + this.getClass());
    }

    public String getIsOnMessage() {
        return isOnMessage;
    }

    public void setIsOnMessage(String isOnMessage) {
        this.isOnMessage = isOnMessage;
    }


}
