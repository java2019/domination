package Model.Entities;

import Model.SlickComponents.ComposantAbstrait;
import org.newdawn.slick.Graphics;
import org.newdawn.slick.Image;
import org.newdawn.slick.Input;

public class StatusAffichage extends ComposantAbstrait {

    /**
     * Constructeur faisant appel à celui de la classe mère ComposantAbstrait, lui passant les informations requises pour fonctionner
     *
     * @param imageFond
     * @param posX
     * @param posY
     * @param input
     */
    public StatusAffichage(Image imageFond, int posX, int posY, Input input) {
        super(imageFond, posX, posY, input);
        super.isOnMessage = "click sur status";
        super.stringOut = "Appuyer sur Enter pour \nlancer le jeu en console";
        super.fontCoeff = 2;
    }


}
