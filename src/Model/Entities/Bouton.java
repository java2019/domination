package Model.Entities;

import Model.SlickComponents.ComposantAbstrait;
import org.newdawn.slick.Image;
import org.newdawn.slick.Input;

/**
 * Composant bouton simple
 */
public class Bouton extends ComposantAbstrait {

    private String nom;

    public Bouton(Image imageFond, int posX, int posY, Input input, String nom) {
        super(imageFond, posX, posY, input);
        this.isOnMessage = "BOUTON " + nom;
        this.isClickedMessage = "";
        this.nom = nom;
    }

    public String getNom() {
        return nom;
    }


}
