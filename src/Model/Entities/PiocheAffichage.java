package Model.Entities;


import Model.SlickComponents.ComposantAbstrait;
import org.newdawn.slick.Graphics;
import org.newdawn.slick.Image;
import org.newdawn.slick.Input;

import java.util.ArrayList;

public class PiocheAffichage extends ComposantAbstrait {

    private ArrayList<Domino> dominosPioche; //Dominos


    public PiocheAffichage(Image imageFond, int posX, int posY, Input userInput) {
        super(imageFond, posX, posY, userInput);
        super.isOnMessage = "Clic sur PiocheAffichage";
        this.dominosPioche = new ArrayList<>();


        piocheTest(3);
    }


    /**
     * Genere et place une pioche de 4 x dominos
     *
     * @param nb
     */
    public void piocheTest(int nb) {

        for (int i = 0; i < nb; i++) {
            dominosPioche.add(randomDomino(0, 0));
            dominosPioche.add(randomDomino(2, 3));
            dominosPioche.add(randomDomino(2, 0));
            dominosPioche.add(randomDomino(0, 3));
        }

    }


    /**
     * Genere un nouveau domino aléatoire (sans prendre en compte les dominos initiaux
     *
     * @return randomDomino
     */
    public Domino randomDomino(int xGrille, int yGrille) {
        SlotAffichage slotA = new SlotAffichage(1 + xGrille, 1 + yGrille, this, input);
        SlotAffichage slotB = new SlotAffichage(1 + xGrille, 2 + yGrille, this, input);
        slotA.setTerrain(Domino.getRandomTerrain());
        slotB.setTerrain(Domino.getRandomTerrain());

        return new Domino(slotA, slotB, input);

    }

    /**
     * Parcours la pioche et affiche le dominos et sa position dans l'interface graphique
     */
    @Override
    public void voir() {
        System.out.println("Main : ");
        for (Domino domino : dominosPioche) {
            System.out.println(domino);

        }
    }

    @Override
    public boolean isOn() {
        if (super.isOn() && input.isMousePressed(0)) {
            this.voir();
            return true;
        }
        return false;
    }

    @Override
    public void render(Graphics graphics) {
        super.render(graphics);

        /*
        Rendu des dominos stockés
        */
        for (Domino domino : dominosPioche) {
            domino.render(graphics);
        }
    }

    @Override
    public String toString() {
        return "PIOCHE";
    }
}

