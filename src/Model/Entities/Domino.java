package Model.Entities;

import Model.SlickComponents.ObserveurInput;
import org.newdawn.slick.Graphics;
import org.newdawn.slick.Input;
import Model.DominationConsole.ListeDominos;
import java.util.HashMap;

public class Domino implements ObserveurInput {


    public final static HashMap<ListeDominos.Terrain, String> enumImageConversion = new HashMap<>();
    private Input input;


    /*"eau", "terre", "champs", "mine", "foret", "montagne", "prairie" : sont les types en string des terrains (comme sur les images
     *qui seront render des slots/faces. Les enums de Terrain sont associés aux String (pour le moment)
     */
    static {
        enumImageConversion.put(ListeDominos.Terrain.CHAMPS, "champs");
        enumImageConversion.put(ListeDominos.Terrain.PRAIRIE, "prairie");
        enumImageConversion.put(ListeDominos.Terrain.FORET, "foret");
        enumImageConversion.put(ListeDominos.Terrain.MONTAGNE, "montagne");
        enumImageConversion.put(ListeDominos.Terrain.MER, "eau");
        enumImageConversion.put(ListeDominos.Terrain.MINE, "mine");
    }

    private SlotAffichage faceA, faceB;
    private int couronne1, couronne2 = 0;


    public Domino(SlotAffichage faceA, SlotAffichage faceB, Input input) {
        this.faceA = faceA;
        this.faceB = faceB;
        this.input = input;

        //this.addImageFaces();

    }

    /**
     * Genere un string de terrain aléatoire
     *
     * @return
     */
    public static String getRandomTerrain() {
        return enumImageConversion.get(ListeDominos.randomFace());
    }

    /**
     * Permet d'afficher facilement les faces d'un domino
     *
     * @return String
     */
    @Override
    public String toString() {
        return "FACE1: " + this.faceA + " / " + this.couronne1 + "c FACE2 : " + this.faceB + " / " + this.couronne2;
    }

    public int getCouronne1() {
        return couronne1;
    }

    public int getCouronne2() {
        return couronne2;
    }

    public SlotAffichage getFaceA() {
        return faceA;
    }

    public SlotAffichage getFaceB() {
        return faceB;
    }


    public void render(Graphics graphics) {
        this.faceA.render(graphics);
        this.faceB.render(graphics);
    }

    @Override
    public void detecteClic() {
        System.out.println("clic detected");

    }
}
