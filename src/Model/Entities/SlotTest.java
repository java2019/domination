package Model.Entities;


import Model.SlickComponents.ComposantAbstrait;
import Model.SlickComponents.SousComposantAbstrait;
import org.newdawn.slick.Graphics;
import org.newdawn.slick.Image;
import org.newdawn.slick.Input;

import java.util.Arrays;

/**
 * Cette classe décrit les slots qui sont les faces de dominos, leur placement dans un composant
 * se fait dans une grille x,y avec des entiers à partir de 0. Ceci rend l'afffichage moins pénible
 * <p>
 * types de Terrain : champs, forêts, mer, pairie, mine, montagne, vide et chateau
 */
public class SlotTest extends SousComposantAbstrait {

    private String terrain;
    public static final String[] TYPES = {"eau", "champs", "mine", "foret", "montagne", "prairie"}; //différents types de terrain pour génération de terrain aléatoire

    public SlotTest(int posX, int posY, ComposantAbstrait composant, Input input) {
        super(posX, posY, composant, input);
        //this.composant.ajoutObservateur(this);

    }

    public String getTerrain() {
        return terrain;
    }


    /**
     * Associe au terrain la bonne image
     *
     * @param terrain
     */
    public void setTerrain(String terrain) {
        this.terrain = terrain;
        boolean typeValid = Arrays.asList(SlotAffichage.TYPES).contains(terrain); //verifie que le terrain est valid avec ceux définis pour le joueur

        //si le terrain est valide, lui assigne la bonne image (mieux que de charger toutes les images pour un seul slot), l'image et uniquement l'image nécessaire est mise en mémoire donc plus cohérent que la version d'avant
        if (typeValid) {
            try {
                this.imageFond = new Image("public/terrains/" + terrain + ".jpg");

            } catch (Exception e) {
                // System.out.println("mauvaise manipulation ou images ne sont pas dans le bon fichier");
                e.printStackTrace();
            }
        } else if (terrain.equals("empty")) {
            System.out.println("ok");
        } else {
            System.out.println("il doit y avoir une erreur quelque part");
        }
    }


    @Override
    public String toString() {
        return "this : " + this.terrain + "{" + posX + "," + posY + "}";
    }


    @Override
    public boolean isOn() {
        if (super.isOn() && input.isMousePressed(3)) {
            System.out.println("on da slot");
        }

        return super.isOn();
    }


    /**
     * posX et posY étant les coordonnées dans la grille du composant, il faut les convertir par rapport au composant et
     * l'interface graphique lui même
     *
     * @param graphics
     */
    @Override
    public void render(Graphics graphics) {
        if (!this.terrain.equals("empty")) {
            int x = composant.getPosX();
            int y = composant.getPosY();
            int xOut, yOut; //les coordonnées finales

            //conversation facile avec posX et posY étant les coordonnées dans le tableau
            xOut = x + posX * 50;
            yOut = y + posY * 50;

            graphics.drawImage(imageFond, xOut, yOut);
        }
    }


    @Override
    public void detecteClic() {
        super.detecteClic();
    }
}