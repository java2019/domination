package Model.Entities;


import Model.SlickComponents.ComposantAbstrait;
import Model.SlickComponents.SousComposantAbstrait;
import org.newdawn.slick.Image;
import org.newdawn.slick.Input;
import org.newdawn.slick.SlickException;

import java.util.Arrays;

/**
 * Cette classe décrit les slots qui sont les faces de dominos, leur placement dans un composant
 * se fait dans une grille x,y avec des entiers à partir de 0. Ceci rend l'afffichage moins pénible
 * <p>
 * types de Terrain : champs, forêts, mer, pairie, mine, montagne, vide et chateau
 */
public class SlotAffichage extends SousComposantAbstrait {

    private String terrain = "empty";
    public static final String[] TYPES = {"eau", "champs", "mine", "foret", "montagne", "prairie"}; //différents types de terrain pour génération de terrain aléatoire


    public SlotAffichage(int posX, int posY, ComposantAbstrait composant, Input input) {
        super(posX, posY, composant, input);
        this.pasAffichage = 50;
        this.isOnMessage = this.toString();

        try {
            Image image = new Image("public/terrains/empty.png");
            this.setImageFond(image);
        } catch (SlickException e) {
            e.printStackTrace();
            //System.out.println("Erreur IMAGE SLOT");
        }
    }
    public String getTerrain() {
        return this.terrain;
    }
    /**
     * Associe au terrain la bonne image
     *
     * @param terrain
     */
    public void setTerrain(String terrain) {
        this.terrain = terrain;
        boolean typeValid = Arrays.asList(SlotAffichage.TYPES).contains(terrain); //verifie que le terrain est valid avec ceux définis pour le joueur

        //si le terrain est valide, lui assigne la bonne image (mieux que de charger toutes les images pour un seul slot), l'image et uniquement l'image nécessaire est mise en mémoire donc plus cohérent que la version d'avant
        if (typeValid) {
            try {
                //System.out.println("Terrain update");
                this.imageFond = new Image("public/terrains/" + this.terrain + ".jpg");


            } catch (Exception e) {
                // System.out.println("mauvaise manipulation ou images ne sont pas dans le bon fichier");
                e.printStackTrace();
            }
        } else {
            //System.out.println("il doit y avoir une erreur quelque part");
        }
    }

    @Override
    public String toString() {
        return "{" + posX + "," + posY + "}" + "[" + composant.toString() + "]";

    }


}