package Model.Entities;

import java.util.*;

import Model.SlickComponents.ComposantAbstrait;
import org.newdawn.slick.Image;
import org.newdawn.slick.Input;

public class PlateauAffichage extends ComposantAbstrait {

    private SlotAffichage[][] grid = new SlotAffichage[11][11];

    private boolean placementPossible;

    /**
     * Cree le PlateauAffichage et le remplit de slots vides
     *
     * @param posX
     * @param posY
     * @param imageFond
     * @param userInput
     */
    public PlateauAffichage(int posX, int posY, Image imageFond, Input userInput) {
        super(imageFond, posX, posY, userInput);
        super.isOnMessage = "Clic sur PlateauAffichage";

        this.placementPossible = true;

        //cree un double array modelisant la grille avec des Slots et leur assigne comme valeur "empty"
        int ligneNb = 0;
        for (SlotAffichage[] ligne : this.grid) {
            for (int i = 0; i < this.grid.length; i++) {
                ligne[i] = new SlotAffichage(i, ligneNb, this, input);
            }
            ligneNb++;
        }


    }


    /**
     * Place un domino Random dans la grille
     */
    public void placerRandom() {
        int randX = new Random().nextInt(11);
        int randY = new Random().nextInt(11);
        int randIndex = new Random().nextInt(SlotAffichage.TYPES.length); //afin d'obtenir un indice pour un type de terrain random (contenu dans SlotAffichage.TYPES)

        modifySlot(randX, randY, SlotAffichage.TYPES[randIndex]);

    }

    /**
     * Permet d'effacer le contenu de la grille soit de faire qu'elle soit vide à l'écran
     */
    public void effacerGrille() {
        observeurs.clear();
        int ligneNb = 0;
        for (SlotAffichage[] ligne : this.grid) {
            for (int i = 0; i < this.grid.length; i++) {
                ligne[i] = new SlotAffichage(i, ligneNb, this, input);
                ligne[i].setTerrain("empty");
                this.ajoutObservateur(ligne[i]);
            }
            ligneNb++;
        }

    }


    /**
     * histoire de voir comment est le PlateauAffichage de manière formatté
     */
    public void logPlateau() {
        //Juste un moyen d'avoir le nombre des lignes et colonnes affichés
        Scanner scanner = new Scanner(Arrays.deepToString(grid).replace("],", "\n"));
        int lineCount = 0;
        String out = "          0         1           2           3           4           5         6         7          8           9           10   \n" +
                "===================================================================================================================================";

        while (scanner.hasNextLine()) {

            out = out + "\n " + String.format("|%3d|", lineCount) + scanner.nextLine();

            lineCount++;
        }
        scanner.close();

        System.out.println(out);

    }

    /**
     * Modifie le String d'un slot
     *
     * @param x
     * @param y
     * @param valeur
     * @return
     */
    private void modifySlot(int x, int y, String valeur) {
        this.grid[x][y].setTerrain(valeur);
    }

    @Override
    public String toString() {
        return "PLATEAU";
    }
}


