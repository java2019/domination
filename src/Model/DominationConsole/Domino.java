package Model.DominationConsole;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.concurrent.ThreadLocalRandom;
import java.util.Collections;

/**
 * @author jeanrannou
 * Cette classe va permettre de generer la liste de dominos de depart et de generer la liste de domino sur laquelle
 * les rois vont se poser a chaque nouveau tour.
 */
public class Domino {
    //Creation d'un dictionnaire et de deux listes : les listes vont contenir les indices qui vont permettre d'aller chercher le domino correspondant dans le dico
    private HashMap dico_domino;
    private ArrayList liste_domino_tour;
    private ArrayList liste_domino_partie;

    /**
     * @param nombre_joueurs Cette classe va permettre de remplir les listes avec le bon nombre d'indices selon le nombre de joueurs.
     */

    public Domino(int nombre_joueurs) {

        //On recupere le dictionnaire contenant les dominos dans la classe ListeDominos
        ListeDominos listeDominos = new ListeDominos();
       // this.dico_domino = ListeDominos;
        //On remplit la liste d'indices de 0 à 47
        for (int i = 0; i < 48; i++) {
            liste_domino_partie.add(i);
        }
        //On reduit la taille de la liste d'indice en fonction du nombre de joueurs
        switch (nombre_joueurs) {
            case 2:
                //Pour 2 joueurs, on garde 24 indices
                while (liste_domino_partie.size() > 24) {
                    //On genere des nombres aleatoires entre 0 et la taille de la liste-1 , et on retire ces indices
                    int randomNum = ThreadLocalRandom.current().nextInt(0, liste_domino_partie.size());
                    liste_domino_partie.remove(randomNum);
                }
                break;

            case 3:
                //Pour 3 joueurs, on garde 36 indices
                while (liste_domino_partie.size() > 36) {
                    int randomNum = ThreadLocalRandom.current().nextInt(0, liste_domino_partie.size());
                    liste_domino_partie.remove(randomNum);
                }
                break;
        }
    }

    /**
     * @param nombre_joueurs Cette classe va recuperer 3 ou 4 indices aleatoires selon le nombre de joueurs dans la liste de tous les indices de dominos
     */
    public void RecupererDomino(int nombre_joueurs) {
        //On cree une liste qui contiendra les indices des dominos du tour en cours
        liste_domino_tour = new ArrayList();
        //Selon le nombre de joueurs, on va generer un nombre different d'indices.
        switch (nombre_joueurs) {
            //Pour 2 joueurs, on genere 4 indices
            case 2:
                for (int i = 0; i < 4; i++) {
                    //
                    int randomNum = ThreadLocalRandom.current().nextInt(0, liste_domino_partie.size());
                    liste_domino_tour.add(liste_domino_partie.get(randomNum));
                    liste_domino_partie.remove(randomNum);
                }
                Collections.sort(liste_domino_tour);
                break;

            //Pour 3 joueurs, on genere 3 indices
            case 3:
                for (int i = 0; i < 3; i++) {
                    int randomNum = ThreadLocalRandom.current().nextInt(0, liste_domino_partie.size());
                    liste_domino_tour.add(liste_domino_partie.get(randomNum));
                    liste_domino_partie.remove(randomNum);
                }
                Collections.sort(liste_domino_tour);

                //Pour 4 joueurs, on genere 4 indices
            case 4:
                for (int i = 0; i < 4; i++) {
                    int randomNum = ThreadLocalRandom.current().nextInt(0, liste_domino_partie.size());
                    liste_domino_tour.add(liste_domino_partie.get(randomNum));
                    liste_domino_partie.remove(randomNum);
                }
                Collections.sort(liste_domino_tour);
        }
    }
}