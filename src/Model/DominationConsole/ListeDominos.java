package Model.DominationConsole;

import java.util.*;

/**
 * Created by Andrew on 12/10/2018.
 */
public class ListeDominos {
    private HashMap<Integer, Model.DominationConsole.FacesDomino> listeTousLesDominos = new HashMap<Integer, Model.DominationConsole.FacesDomino>();

    public ListeDominos() {
        listeTousLesDominos.put(1, new FacesDomino(0, 0, Terrain.CHAMPS, Terrain.CHAMPS));
        listeTousLesDominos.put(2, new FacesDomino(0, 0, Terrain.CHAMPS, Terrain.CHAMPS));
        listeTousLesDominos.put(3, new FacesDomino(0, 0, Terrain.FORET, Terrain.FORET));
        listeTousLesDominos.put(4, new FacesDomino(0, 0, Terrain.FORET, Terrain.FORET));
        listeTousLesDominos.put(5, new FacesDomino(0, 0, Terrain.FORET, Terrain.FORET));
        listeTousLesDominos.put(6, new FacesDomino(0, 0, Terrain.FORET, Terrain.FORET));
        listeTousLesDominos.put(7, new FacesDomino(0, 0, Terrain.MER, Terrain.MER));
        listeTousLesDominos.put(8, new FacesDomino(0, 0, Terrain.MER, Terrain.MER));
        listeTousLesDominos.put(9, new FacesDomino(0, 0, Terrain.MER, Terrain.MER));
        listeTousLesDominos.put(10, new FacesDomino(0, 0, Terrain.PRAIRIE, Terrain.PRAIRIE));
        listeTousLesDominos.put(11, new FacesDomino(0, 0, Terrain.PRAIRIE, Terrain.PRAIRIE));
        listeTousLesDominos.put(12, new FacesDomino(0, 0, Terrain.MINE, Terrain.MINE));
        listeTousLesDominos.put(13, new FacesDomino(0, 0, Terrain.CHAMPS, Terrain.FORET));
        listeTousLesDominos.put(14, new FacesDomino(0, 0, Terrain.CHAMPS, Terrain.MER));
        listeTousLesDominos.put(15, new FacesDomino(0, 0, Terrain.CHAMPS, Terrain.PRAIRIE));
        listeTousLesDominos.put(16, new FacesDomino(0, 0, Terrain.CHAMPS, Terrain.MINE));
        listeTousLesDominos.put(17, new FacesDomino(0, 0, Terrain.FORET, Terrain.MER));
        listeTousLesDominos.put(18, new FacesDomino(0, 0, Terrain.FORET, Terrain.PRAIRIE));
        listeTousLesDominos.put(19, new FacesDomino(1, 0, Terrain.CHAMPS, Terrain.FORET));
        listeTousLesDominos.put(20, new FacesDomino(1, 0, Terrain.CHAMPS, Terrain.MER));
        listeTousLesDominos.put(21, new FacesDomino(1, 0, Terrain.CHAMPS, Terrain.PRAIRIE));
        listeTousLesDominos.put(22, new FacesDomino(1, 0, Terrain.CHAMPS, Terrain.MINE));
        listeTousLesDominos.put(23, new FacesDomino(1, 0, Terrain.CHAMPS, Terrain.MONTAGNE));
        listeTousLesDominos.put(24, new FacesDomino(1, 0, Terrain.FORET, Terrain.CHAMPS));
        listeTousLesDominos.put(25, new FacesDomino(1, 0, Terrain.FORET, Terrain.CHAMPS));
        listeTousLesDominos.put(26, new FacesDomino(1, 0, Terrain.FORET, Terrain.CHAMPS));
        listeTousLesDominos.put(27, new FacesDomino(1, 0, Terrain.FORET, Terrain.CHAMPS));
        listeTousLesDominos.put(28, new FacesDomino(1, 0, Terrain.FORET, Terrain.MER));
        listeTousLesDominos.put(29, new FacesDomino(1, 0, Terrain.FORET, Terrain.PRAIRIE));
        listeTousLesDominos.put(30, new FacesDomino(1, 0, Terrain.MER, Terrain.CHAMPS));
        listeTousLesDominos.put(31, new FacesDomino(1, 0, Terrain.MER, Terrain.CHAMPS));
        listeTousLesDominos.put(32, new FacesDomino(1, 0, Terrain.MER, Terrain.FORET));
        listeTousLesDominos.put(33, new FacesDomino(1, 0, Terrain.MER, Terrain.FORET));
        listeTousLesDominos.put(34, new FacesDomino(1, 0, Terrain.MER, Terrain.FORET));
        listeTousLesDominos.put(35, new FacesDomino(1, 0, Terrain.MER, Terrain.FORET));
        listeTousLesDominos.put(36, new FacesDomino(0, 1, Terrain.CHAMPS, Terrain.PRAIRIE));
        listeTousLesDominos.put(37, new FacesDomino(0, 1, Terrain.MER, Terrain.PRAIRIE));
        listeTousLesDominos.put(38, new FacesDomino(0, 1, Terrain.CHAMPS, Terrain.MINE));
        listeTousLesDominos.put(39, new FacesDomino(0, 1, Terrain.PRAIRIE, Terrain.MINE));
        listeTousLesDominos.put(40, new FacesDomino(1, 0, Terrain.MONTAGNE, Terrain.CHAMPS));
        listeTousLesDominos.put(41, new FacesDomino(0, 2, Terrain.CHAMPS, Terrain.PRAIRIE));
        listeTousLesDominos.put(42, new FacesDomino(0, 2, Terrain.MER, Terrain.PRAIRIE));
        listeTousLesDominos.put(43, new FacesDomino(0, 2, Terrain.CHAMPS, Terrain.MINE));
        listeTousLesDominos.put(44, new FacesDomino(0, 2, Terrain.PRAIRIE, Terrain.MINE));
        listeTousLesDominos.put(45, new FacesDomino(2, 0, Terrain.MONTAGNE, Terrain.CHAMPS));
        listeTousLesDominos.put(46, new FacesDomino(0, 2, Terrain.MINE, Terrain.MONTAGNE));
        listeTousLesDominos.put(47, new FacesDomino(0, 2, Terrain.MINE, Terrain.MONTAGNE));
        listeTousLesDominos.put(48, new FacesDomino(0, 3, Terrain.CHAMPS, Terrain.MONTAGNE));
    }

    /**
     * Genere une Terrain aléatoire sans terrain VIDE
     *
     * @return randomFace
     */
    public static Terrain randomFace() {
        List<Terrain> valeurs = Collections.unmodifiableList(Arrays.asList(Terrain.values()));
        Terrain terrainAleat;
        do {
            terrainAleat = valeurs.get(new Random().nextInt(valeurs.size()));
        } while (terrainAleat == Terrain.VIDE);
        return terrainAleat;
    }

    public HashMap<Integer, FacesDomino> getListeTousLesDominos() {
        return listeTousLesDominos;
    }


    public enum Terrain {
        CHAMPS,
        FORET,
        MER,
        PRAIRIE,
        MINE,
        MONTAGNE,
        VIDE,
        CHATEAU
    }
}
