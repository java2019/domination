package Model.DominationConsole;
/**
 * Created by Andrew on 12/10/2018.
 */

/**
 * CLASS QUI DECRIT LA FACE D'UN DOMINO
 */
public class FacesDomino {

    private int couronne1,couronne2;
    private ListeDominos.Terrain terrain1,terrain2; //enum pour les types de terrain

    /**
     * Constructeur des faces d'un domino
     * @param couronne1 nombre de couronnes sur face1
     * @param couronne2 nombre de couronnes sur face2
     * @param terrain1 type de terrain sur face 1
     * @param terrain2 type de terrain sur face 1
     */
    public FacesDomino(int couronne1, int couronne2, ListeDominos.Terrain terrain1, ListeDominos.Terrain terrain2) {
        this.couronne1 = couronne1;
        this.couronne2 = couronne2;
        this.terrain1 = terrain1;
        this.terrain2 = terrain2;
    }

    /**
     * Permet d'afficher facilement les faces d'un domino
     * @return String
     */
    @Override
    public String toString() {
        return "FACE1: "+this.terrain1+" / "+this.couronne1+"c FACE2 : "+this.terrain2+" / "+this.couronne2;
    }

    public int getCouronne1() {
        return couronne1;
    }

    public int getCouronne2() {
        return couronne2;
    }

    public ListeDominos.Terrain getTerrain1() {
        return terrain1;
    }

    public ListeDominos.Terrain getTerrain2() {
        return terrain2;
    }
}
