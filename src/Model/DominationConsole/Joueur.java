package Model.DominationConsole;

public class Joueur {
    private String nomDuJoueur;
    private int ordreRoi;
    private int ordreRoi2;
    private int nombreJeuTour = 1;
    private Plateau plateauJoueur;
    private int scoreJoueur;


    public Joueur(String nom_du_joueur){
        this.nomDuJoueur = nom_du_joueur;
        this.plateauJoueur = new Plateau("plateau");
    }

    public int getOrdreRoi() {
        return ordreRoi;
    }

    public void setOrdreRoi(int ordreRoi) {
        this.ordreRoi = ordreRoi;
    }

    public int getOrdreRoi2() {
        return ordreRoi2;
    }

    public void setOrdreRoi2(int ordreRoi2) {
        this.ordreRoi2 = ordreRoi2;
    }

    public Plateau getPlateauJoueur() {
        return plateauJoueur;
    }

    public String getNomDuJoueur() {
        return nomDuJoueur;
    }

    public int getScoreJoueur() {
        return scoreJoueur;
    }

    public void setScoreJoueur(int scoreJoueur) {
        this.scoreJoueur = scoreJoueur;
    }
    public int getNombreJeuTour() {
        return nombreJeuTour;
    }

    public void setNombreJeuTour(int nombreJeuTour) {
        this.nombreJeuTour = nombreJeuTour;
    }

}