package Model.DominationConsole;


import java.util.Arrays;


/**
 * Gestion du plateau
 */
public class Plateau {
    Slot[][] grid;
    int[][] tableauCouronne = new int[11][11];
    private String gridName;

    /**
     * Crée le plateau initial 9x9
     * @param gridName nom du plateau
     */
    public Plateau(String gridName) {
        this.gridName = gridName;
        this.grid = new Slot[11][11];
        for (Slot[] ligne : this.grid) {
            for (int i = 0; i < this.grid.length; i++) {
                ligne[i] = new Slot();
            }
        }
        this.grid[5][5].setTerrain(ListeDominos.Terrain.CHATEAU);
    }

    /**
     * Calcule le nombre de couronnes sur le plateau
     *
     * @return nombre de couronnes
     */
    public int nombreCouronnes() {
        int nombreCouronne = 0;
        for (int[] ligne : tableauCouronne) {
            for (int couronne : ligne) {
                nombreCouronne += couronne;
            }
        }
        return nombreCouronne;
    }

    /**
     * Affiche le plateau de couronne
     */
    public void afficherCouronnes() {
        for (int i = 0; i <= 10; i++) {

            if (i == 0) {
                for (int k = 0; k <= 10; k++) {
                    System.out.print("     " + k);
                }
                System.out.println();
            }

            for (int j = 0; j <= 10; j++) {
                System.out.print("     " + tableauCouronne[i][j]);
            }
            System.out.print("  " + i + "\n");
        }
    }

    /**
     * Détermine si le chateau est au milieu du plateau
     * @return true si le joueur à construit son plateau avec le chateau au centre
     */
    public boolean chateauAuMilieu() {
        boolean presenceGauche = false;
        boolean presenceDroite = false;
        boolean presenceHaut = false;
        boolean presenceBas = false;
        int j = 3;
        while (j < 8) {
            if (!(this.grid[3][j].getTerrain() == ListeDominos.Terrain.VIDE)) {
                presenceHaut = true;
            }
            if (!(this.grid[7][j].getTerrain() == ListeDominos.Terrain.VIDE)) {
                presenceBas = true;
            }
            if (!(this.grid[j][3].getTerrain() == ListeDominos.Terrain.VIDE)) {
                presenceGauche = true;
            }
            if (!(this.grid[j][7].getTerrain() == ListeDominos.Terrain.VIDE)) {
                presenceDroite = true;
            }
            j++;
        }
        if ((presenceGauche == true && presenceDroite == true && presenceBas == true && presenceHaut == true)) {
            return (true);
        } else {
            return (false);
        }
    }

    /**
     * Permet de connaitre le nombre de cases remplies
     *
     * @return nombre de cases non vide sur le plateau
     */
    public int nombreCasesRemplies() {
        int nombreCasesRemplies = 0;
        for (int i = 0; i == 10; i++) {
            for (int j = 0; j == 10; j++) {
                if (this.grid[i][j].getTerrain() != (ListeDominos.Terrain.VIDE)) {
                    nombreCasesRemplies += 1;
                }
            }
        }
        return nombreCasesRemplies;
    }

    /**
     * Fait les vérification et place le domino
     * @param facesDomino informations du domino
     * @param ligneX emplacement x de la face1
     * @param colY emplacement y de la face1
     * @param orientation orientation du domino par rapport à la face 1
     * @return
     */
    public boolean placer(FacesDomino facesDomino, int ligneX, int colY, char orientation) {
        boolean bienPlace = false;
        int ligneX2 = 0;
        int colY2 = 0;

        ListeDominos.Terrain face1 = facesDomino.getTerrain1();
        ListeDominos.Terrain face2 = facesDomino.getTerrain2();
        int couronne1 = facesDomino.getCouronne1();
        int couronne2 = facesDomino.getCouronne2();

        switch (orientation) {
            case 'u':
                ligneX2 = ligneX - 1;
                colY2 = colY;
                break;
            case 'd':
                ligneX2 = ligneX + 1;
                colY2 = colY;
                break;
            case 'l':
                ligneX2 = ligneX;
                colY2 = colY - 1;
                break;
            case 'r':
                ligneX2 = ligneX;
                colY2 = colY + 1;
                break;
        }


        if (emplacementVide(ligneX, colY, ligneX2, colY2)) {
            if (terrainAdjacent(face1, ligneX, colY, face2, ligneX2, colY2)) {
                if (dansTerrain(face1, ligneX, colY, face2, ligneX2, colY2)) {
                    bienPlace = true;
                    this.grid[ligneX][colY].setTerrain(face1);
                    tableauCouronne[ligneX][colY] = couronne1;
                    this.grid[ligneX2][colY2].setTerrain(face2);
                    tableauCouronne[ligneX2][colY2] = couronne2;
                } else {
                    System.out.println("Hors du terrain");
                    bienPlace = false;
                }
            } else {
                System.out.println("Pas de dominos adjacent");
                bienPlace = false;
            }
        } else {
            System.out.println("Emplacement non vide");
            bienPlace = false;
        }
        return bienPlace;
    }

    /**
     * détermine si l'emplacement ne comporte pas déjà soit un terrain soit un chateau
     *
     * @param ligneX  emplacement x de la première face
     * @param colY    emplacement y de la première face
     * @param ligneX2 emplacement x de la deuxième face
     * @param colY2   emplacement y de la deuxième face
     * @return true si l'emplacement est vide
     */
    private boolean emplacementVide(int ligneX, int colY, int ligneX2, int colY2) {
        boolean vide = false;
        if (this.grid[ligneX][colY].getTerrain() == ListeDominos.Terrain.VIDE) {
            if (this.grid[ligneX2][colY2].getTerrain() == ListeDominos.Terrain.VIDE) {
                vide = true;
            }
        } else {
            vide = false;
        }
        return vide;
    }

    /**
     * determine s'il y a au moins 1 terrain similaire adjacent
     */
    private boolean terrainAdjacent(ListeDominos.Terrain face1, int ligneX, int colY, ListeDominos.Terrain face2, int ligneX2, int colY2) {
        boolean adj;
        if ((this.grid[ligneX - 1][colY].getTerrain() == face1) || (this.grid[ligneX - 1][colY].getTerrain() == ListeDominos.Terrain.CHATEAU)
                || (this.grid[ligneX][colY + 1].getTerrain() == face1) || (this.grid[ligneX][colY + 1].getTerrain() == ListeDominos.Terrain.CHATEAU)
                || (this.grid[ligneX + 1][colY].getTerrain() == face1) || (this.grid[ligneX + 1][colY].getTerrain() == ListeDominos.Terrain.CHATEAU)
                || (this.grid[ligneX][colY - 1].getTerrain() == face1) || (this.grid[ligneX][colY - 1].getTerrain() == ListeDominos.Terrain.CHATEAU)

                || (this.grid[ligneX2 - 1][colY2].getTerrain() == face2) || (this.grid[ligneX2 - 1][colY2].getTerrain() == ListeDominos.Terrain.CHATEAU)
                || (this.grid[ligneX2][colY2 + 1].getTerrain() == face2) || (this.grid[ligneX2][colY2 + 1].getTerrain() == ListeDominos.Terrain.CHATEAU)
                || (this.grid[ligneX2 + 1][colY2].getTerrain() == face2) || (this.grid[ligneX2 + 1][colY2].getTerrain() == ListeDominos.Terrain.CHATEAU)
                || (this.grid[ligneX2][colY2 - 1].getTerrain() == face2) || (this.grid[ligneX2][colY2 - 1].getTerrain() == ListeDominos.Terrain.CHATEAU)) {
            adj = true;
        } else {
            adj = false;
        }
        return adj;
    }

    /**
     * vérifie que le terrain ne depasse pas 5*5
     *
     * @param face1   face 1
     * @param ligneX  emplacement x de la première face
     * @param colY    emplacement y de la première face
     * @param face2   face 2
     * @param ligneX2 emplacement x de la deuxième face
     * @param colY2   emplacement y de la deuxième face
     * @return true si le domino est placé dans le terrain
     */
    private boolean dansTerrain(ListeDominos.Terrain face1, int ligneX, int colY, ListeDominos.Terrain face2, int ligneX2, int colY2) {
        boolean bienPlace = true;
        if ((ligneX < 1) || (ligneX > 9) || (colY < 1) || (colY > 9) || (ligneX2 < 1) || (ligneX2 > 9) || (colY2 < 1) || (colY2 > 9)) {
            bienPlace = false;
            return bienPlace;
        }

        int nombreLignesDomino = 0;
        int nombreColonnesDomino = 0;

        boolean faceHorizontalePresente = false;
        boolean faceVerticalePresente = false;

        for (int i = 0; i < 11; i++) {
            int j = 0;
            while (j < 11 && (faceHorizontalePresente == false)) {
                if (this.grid[i][j].getTerrain() != ListeDominos.Terrain.VIDE || i == ligneX || i == ligneX2) {
                    faceHorizontalePresente = true;
                }
                if (faceHorizontalePresente == true) {
                    nombreLignesDomino += 1;
                }
                j += 1;
            }
            j = 0;
            while (j < 11 && (faceVerticalePresente == false)) {
                if (this.grid[j][i].getTerrain() != ListeDominos.Terrain.VIDE || i == colY || i == colY2) {
                    faceVerticalePresente = true;
                }
                if (faceVerticalePresente == true) {
                    nombreColonnesDomino += 1;
                }
                j += 1;
            }
            faceVerticalePresente = false;
            faceHorizontalePresente = false;
        }

        if ((nombreLignesDomino > 5) || (nombreColonnesDomino > 5)) {
            bienPlace = false;
            return bienPlace;
        }
        return bienPlace;
    }


    @Override
    public String toString() {
        //partie du haut avec indices et ligne
        String out = String.format("%5s", " ");
        for (int z = 0; z < 2; z++) {
            for (int i = 0; i < this.grid.length; i++) {
                if (z == 0) {
                    out += String.format("%15s", i);
                    if (i == grid.length - 1) out += "\n";
                } else {
                    if (i == 0) out += String.format("%5s", "-").replace(" ", "-");
                    out += String.format("%15s", "-").replace(" ", "-");
                }
            }
            if (z == 1) out += "\n";
        }

        //cree un double array modelisant la grille avec des Slots et leur assigne comme valeur "empty"

        for (int y = 0; y < this.grid.length; y++) {
            for (int x = 0; x < this.grid.length; x++) {

                if (x == 0) out += String.format("%5s", y + "  |");
                out += String.format("%15s", grid[x][y].toString() + "[" + x + "," + y + "]");
            }
            out += "\n";

        }
        return out;
    }


    /**
     * chaque element de la grille tableau est un slot qui comporte du terrain
     * On peut récuprer la valeur du slot et la changer
     */
    class Slot {
        ListeDominos.Terrain terrain;

        /**
         * creation d'un slot libre lors de l'instanciation
         */
        public Slot() {
            this.terrain = ListeDominos.Terrain.VIDE;
        }

        public ListeDominos.Terrain getTerrain() {
            return terrain;
        }

        public void setTerrain(ListeDominos.Terrain terrain) {
            this.terrain = terrain;
        }



        @Override
        public String toString() {
            return terrain.toString();
        }
    }
}

