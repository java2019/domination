package Model.DominationConsole;

import java.util.*;
import java.util.concurrent.ThreadLocalRandom;

/**
 * Classe qui permet le déroulement de la partie
 */
public class Jeu {
    private ArrayList<Joueur> listeJoueur = new ArrayList<Joueur>();
    private int nombreDeJoueurs;
    private boolean encours;
    private ArrayList<Integer> listeDominoPartie = new ArrayList();
    private ArrayList<Integer> listeDominoTour = new ArrayList();
    private HashMap<Integer, FacesDomino> listeTousLesDominos = new HashMap<>();
    private int scoreTemporaire = 0;
    private int couronne = 0;
    private HashMap<Integer, Integer> tableauX = new HashMap<Integer, Integer>();
    private HashMap<Integer, Integer> tableauY = new HashMap<Integer, Integer>();
    private ArrayList<Joueur> listeScoreJoueur = new ArrayList();

    /**
     * Création du jeu = création de la liste des Dominos
     */
    public Jeu() {
        this.encours = true;
        ListeDominos listedom = new ListeDominos();
        this.listeTousLesDominos = listedom.getListeTousLesDominos();
    }

    /**
     * Moyen de passer le tout à la vue en utilisant le Singleton
     *
     * @return
     */
    public int getNombreDeJoueurs() {
        return nombreDeJoueurs;
    }

    /**
     * Moyen qui permet de prendre des données de la vue
     * @param nombreDeJoueurs
     */
    public void setNombreDeJoueurs(int nombreDeJoueurs) {
        this.nombreDeJoueurs = nombreDeJoueurs;
    }

    /**
     * Ajoute les joueurs au jeu
     *
     * @param noms //ArrayList données par la vue (ChoixPseudoState)
     */
    public void addJoueurs2(ArrayList<String> noms) {
        for (String nom : noms) {
            this.listeJoueur.add(new Joueur(nom));
        }
    }

    /**
     * Demande le nombre de joueur et l'ajoute à la variable de classe nombreDeJoueurs
     */
    public void nombreDeJoueurs() {
        Scanner scan = new Scanner(System.in);
        int choixNombreDeJoueurs = 0;
        do {
            System.out.println("Combien de joueur ?");
            try {
                choixNombreDeJoueurs = scan.nextInt();
            } catch (Exception e) {
                System.out.println("nombre non valide");
                scan.nextLine();
            }
        } while (choixNombreDeJoueurs < 2 || choixNombreDeJoueurs > 4);
        scan.nextLine();
        this.nombreDeJoueurs = choixNombreDeJoueurs;
    }

    /**
     * Créée un nouveau joueur et l'ajoute à la liste des joueurs de la partie
     */
    public void ajoutJoueur() {
        Scanner scan = new Scanner(System.in);
        for (int i = 0; i < nombreDeJoueurs; i++) {
            System.out.println("Quel pseudo ?");
            String nomDuJoueur = scan.nextLine();
            Joueur joueur = new Joueur(nomDuJoueur);
            listeJoueur.add(joueur);
        }
    }

    /**
     * Créée une liste de domino pour la partie selon le nombre de joueur
     */
    public void creationListeDominoPartie() {
        for (int i = 1; i < 48; i++) {
            listeDominoPartie.add(i);
        }
        switch (nombreDeJoueurs) {
            case 2:
                //Pour 2 joueurs, on garde 24 indices
                while (listeDominoPartie.size() > 24) {
                    //On genere des nombres aleatoires entre 0 et la taille de la liste-1 , et on retire ces indices
                    int randomNum = ThreadLocalRandom.current().nextInt(0, listeDominoPartie.size());
                    listeDominoPartie.remove(randomNum);
                }
                break;

            case 3:
                //Pour 3 joueurs, on garde 36 indices
                while (listeDominoPartie.size() > 36) {
                    int randomNum = ThreadLocalRandom.current().nextInt(0, listeDominoPartie.size());
                    listeDominoPartie.remove(randomNum);
                }
                break;
        }
    }

    /**
     * Détermine un ordre de départ aléatoire pour chaque joueur
     */
    public void ordreDeDepart() {
        Random random = new Random();

        int ran;
        if (nombreDeJoueurs == 2) {
            int[] listeIndice = new int[4];
            int[] listeIndice1 = new int[4];
            for (int i = 0; i < 4; i++) {
                listeIndice[i] = i;
            }
            for (int i = 0; i < 4; i++) {
                do {
                    ran = random.nextInt(listeIndice.length);
                } while (listeIndice[ran] == -1);
                listeIndice1[i] = ran;
                listeIndice[ran] = -1;
            }
            listeJoueur.get(0).setOrdreRoi(listeIndice1[0]);
            listeJoueur.get(0).setOrdreRoi2(listeIndice1[1]);
            listeJoueur.get(1).setOrdreRoi(listeIndice1[2]);
            listeJoueur.get(1).setOrdreRoi2(listeIndice1[3]);
        } else {
            int[] listeIndice = new int[nombreDeJoueurs];
            for (int i = 0; i < nombreDeJoueurs; i++) {
                listeIndice[i] = i;
            }
            for (int i = 0; i < nombreDeJoueurs; i++) {
                do {
                    ran = random.nextInt(listeIndice.length);
                } while (listeIndice[ran] == -1);
                listeJoueur.get(i).setOrdreRoi(ran);
                listeIndice[ran] = -1;
            }
        }
    }

    /**
     * Retourne le status du jeu : en cours
     * La partie s'arrête quand tous les dominos ont été placé
     * @return Boolean encours
     */
    public boolean isEncours() {
        if (listeDominoPartie.size() == 0) {
            this.encours = false;
        }
        return encours;
    }

    /**
     * Détermine l'ordre du tour selon les rois (attribut des Joueurs)
     * @return tableau avec l'ordre d'appel des joueurs
     */
    public int[] ordreTour() {
        if (nombreDeJoueurs == 2) {
            int tableauOrdre[] = new int[4];
            int tableauOrdreBis[] = new int[4];
            int i = 0;
            //Stock dans une liste l'ordre de chacun des joueurs
            for (Joueur joueur : listeJoueur) {
                tableauOrdre[i] = joueur.getOrdreRoi();
                tableauOrdre[i + 1] = joueur.getOrdreRoi2();
                i += 2;
            }

            //Détermine l'ordre d'appel des joueurs
            for (int j = 0; j < 4; j++) {
                i = 0;
                while (tableauOrdre[i] != j) {
                    i++;
                }
                if (i < 2) {
                    tableauOrdreBis[j] = 0;
                } else {
                    tableauOrdreBis[j] = 1;
                }
            }
            return (tableauOrdreBis);
        } else {
            int tableauOrdre[] = new int[nombreDeJoueurs];
            int tableauOrdreBis[] = new int[nombreDeJoueurs];
            int i = 0;
            //Stock dans une liste l'ordre de chacun des joueurs
            for (Joueur joueur : listeJoueur) {
                tableauOrdre[i] = joueur.getOrdreRoi();
                i++;
            }
            //Détermine l'ordre d'appel des joueurs
            for (int j = 1; j < nombreDeJoueurs + 1; j++) {
                i = 0;
                while (tableauOrdre[i] != j - 1) {
                    i++;
                }
                tableauOrdreBis[j - 1] = i;
            }
            return (tableauOrdreBis);
        }
    }

    /**
     * Fait jouer les joueurs dans l'ordre du tour
     * @param tableauOrdre l'ordre du tour
     */
    public void tour(int tableauOrdre[]) {
        genererDominoTour();
        for (int indice = 0; indice < tableauOrdre.length; indice++) {
            tourJoueur(listeJoueur.get(tableauOrdre[indice]));
        }
    }

    /**
     * Prends 4 dominos aléatoire dans la liste des dominos de la partie
     */
    private void genererDominoTour() {
        //Selon le nombre de joueurs, on va generer un nombre different d'indices.
        listeDominoTour.clear();
        switch (nombreDeJoueurs) {
            //Pour 2 joueurs, on genere 4 indices
            case 2:
                for (int i = 0; i < 4; i++) {
                    int randomNum = ThreadLocalRandom.current().nextInt(0, listeDominoPartie.size());
                    listeDominoTour.add(listeDominoPartie.get(randomNum));
                    listeDominoPartie.remove(randomNum);
                }
                Collections.sort(listeDominoTour);
                break;
            //Pour 3 joueurs, on genere 3 indices
            case 3:
                for (int i = 0; i < 3; i++) {
                    int randomNum = ThreadLocalRandom.current().nextInt(0, listeDominoPartie.size());
                    listeDominoTour.add(listeDominoPartie.get(randomNum));
                    listeDominoPartie.remove(randomNum);
                }
                Collections.sort(listeDominoTour);
                break;

            //Pour 4 joueurs, on genere 4 indices
            case 4:
                for (int i = 0; i < 4; i++) {
                    int randomNum = ThreadLocalRandom.current().nextInt(0, listeDominoPartie.size());
                    listeDominoTour.add(listeDominoPartie.get(randomNum));
                    listeDominoPartie.remove(randomNum);
                }
                Collections.sort(listeDominoTour);
        }
    }

    /**
     * gère le tour du joueur en entier, du choix du domino à l'affichage du score
     * @param joueur Joueur qui va jouer son tour
     */
    private void tourJoueur(Joueur joueur) {
        Scanner scan = new Scanner(System.in);
        System.out.println(" \n \n" + "C'est à " + joueur.getNomDuJoueur() + " de jouer");
        joueur.getPlateauJoueur().afficherCouronnes();
        System.out.println(joueur.getPlateauJoueur());

        //Choix du domino
        int choixDomino = choixDomino();
        switch (nombreDeJoueurs) {
            case 2:
                switch (joueur.getNombreJeuTour()) {
                    case 1:
                        joueur.setOrdreRoi(choixDomino);
                        joueur.setNombreJeuTour(2);
                        break;
                    case 2:
                        joueur.setOrdreRoi2(choixDomino);
                        joueur.setNombreJeuTour(1);
                        break;
                }
                break;
            default:
                joueur.setOrdreRoi(choixDomino);
        }
        int domino = listeDominoTour.get(Integer.valueOf(choixDomino));
        listeDominoTour.set(choixDomino, -1);
        System.out.println("Vous avez choisi le domino en position : " + choixDomino);

        //Boucle de placement
        int coordonneeLigne = -1;
        int coordonneeColonne = -1;
        char orientation = 'o';
        boolean place = false;
        boolean supprimer = false;
        char choix = 'p';
        do {
            //Supression du domino
            System.out.println("Voulez vous supprimer le domino ? O/N");
            try {
                choix = scan.nextLine().toLowerCase().charAt(0);
            } catch (Exception e) {
            }
            if (choix == 'o') {
                supprimer = true;
            } else {

                //Demande Ligne
                do {
                    System.out.println("Quelle ligne ?");
                    try {
                        coordonneeLigne = scan.nextInt();
                    } catch (Exception e) {
                        System.out.println("nombre non valide");
                        scan.nextLine();
                    }
                } while (coordonneeLigne > 11 || coordonneeLigne < 0);

                //Demande Colonne
                do {
                    System.out.println("Quelle colonne ?");
                    try {
                        coordonneeColonne = scan.nextInt();
                    } catch (Exception e) {
                        System.out.println("nombre non valide");
                        scan.nextLine();

                    }
                } while (coordonneeColonne > 11 || coordonneeColonne < 0);

                //Demande orientation
                scan.nextLine();
                do {
                    System.out.println("Quelle orientation ?");
                    try {
                        orientation = scan.nextLine().charAt(0);
                    } catch (Exception e) {
                        System.out.println("caractere nécessaire");
                    }
                } while (!(orientation == 'u' || orientation == 'd' || orientation == 'r' || orientation == 'l'));

                place = joueur.getPlateauJoueur().placer(listeTousLesDominos.get(domino), coordonneeLigne, coordonneeColonne, orientation);
            }
        }
        while (!place && !supprimer);
        joueur.setScoreJoueur(calculScore(joueur.getPlateauJoueur()));
        System.out.println("votre score est " + joueur.getScoreJoueur());
    }

    /**
     * Demande à l'utilisateur de choisir un domino parmis ceux restant pour le tour
     * @return choix de l'utilisateur
     */
    private int choixDomino() {
        afficherDomino();
        int indiceDomino;
        Scanner scan = new Scanner(System.in);
        indiceDomino = -1;
        do {
            System.out.println("Quel domino choisissez vous ? (indice)");
            try {
                indiceDomino = scan.nextInt();

            } catch (Exception e) {
                System.out.println("Ce n'est pas un numéro valide");
                scan.nextLine();
            }
        }
        while (indiceDomino < 0 || indiceDomino > listeDominoTour.size() - 1 || listeDominoTour.get(Integer.valueOf(indiceDomino)) == -1);
        return (indiceDomino);
    }

    /**
     * Affiche les dominos du tour dans la console
     */
    private void afficherDomino() {
        System.out.println();
        for (int domino : listeDominoTour) {
            if (domino != -1) {
                System.out.print(domino + " : face 1 - " + listeTousLesDominos.get(domino).getTerrain1() + ", " + listeTousLesDominos.get(domino).getCouronne1());
                System.out.println(" | face 2 - " + listeTousLesDominos.get(domino).getTerrain2() + ", " + listeTousLesDominos.get(domino).getCouronne2());
            } else {
                System.out.println("-1");
            }
        }
    }

    /**
     * Calcul le score (nbdeCase*nbDeCouronne pour chaque zone)
     * @param plateau Le plateau du joueur
     * @return Score du joueur
     */
    private int calculScore(Plateau plateau) {
        int score = 0;
        int indiceRemplissageTableau;
        int indiceLimiteEnumTerrain = 0;
        for (ListeDominos.Terrain type : ListeDominos.Terrain.values()) {
            indiceRemplissageTableau = 0;
            if (indiceLimiteEnumTerrain < 6) {
                for (int i = 0; i < 11; i++) {
                    for (int j = 0; j < 11; j++) {
                        if (plateau.grid[i][j].getTerrain() == type) {
                            tableauX.put(indiceRemplissageTableau, i);
                            tableauY.put(indiceRemplissageTableau, j);
                            indiceRemplissageTableau++;
                        }
                    }
                }
                indiceLimiteEnumTerrain++;
                while (tableauX.size() != 0) {
                    scoreTemporaire = 0;
                    couronne = 0;
                    int lol = (Integer) tableauX.keySet().toArray()[0];
                    calculScore2(lol, tableauX.get(lol), tableauY.get(lol), plateau);
                    score = score + scoreTemporaire * couronne;
                }
            }
        }
        return (score);
    }

    /**
     * Permet de calculer le score en explorant les cases autour
     * @param indice Numéro du domino
     * @param x emplacement x
     * @param y emplacement y
     * @param plateau plateau du joueur
     */
    private void calculScore2(int indice, int x, int y, Plateau plateau) {
        couronne += plateau.tableauCouronne[x][y];
        tableauX.remove(indice);
        tableauY.remove(indice);
        scoreTemporaire++;
        for (int cle = 0; cle < 60; cle++) {
            if (tableauX.containsKey(cle)) {
                int xTemporaire = tableauX.get(cle);
                int yTemporaire = tableauY.get(cle);
                if ((xTemporaire == x + 1 && yTemporaire == y) ||
                        (xTemporaire == x - 1 && yTemporaire == y) ||
                        (xTemporaire == x && yTemporaire == y - 1) ||
                        (xTemporaire == x && yTemporaire == y + 1)) {
                    calculScore2(cle, xTemporaire, yTemporaire, plateau);
                }
            }
        }
    }

    /**
     * Calcul le score en fin de partie et l'affiche avec les bonus
     */
    public void finDePartie() {
        for (Joueur joueur : listeJoueur) {
            if (joueur.getPlateauJoueur().chateauAuMilieu()) {
                int score = joueur.getScoreJoueur();
                joueur.setScoreJoueur(score + 10);
            }
            if (joueur.getPlateauJoueur().nombreCasesRemplies() == 25) {
                int score = joueur.getScoreJoueur();
                joueur.setScoreJoueur((score + 5));
            }
        }
        //Affiche tous les scores
        System.out.println("\n \n");
        for (Joueur joueur : listeJoueur) {
            System.out.println(joueur.getNomDuJoueur() + " a fait : " + joueur.getScoreJoueur() + " points");
        }
        System.out.println("\n");

        //Dapartage les vainqueurs
        int score = topScore();
        if (listeScoreJoueur.size() > 1) {
            topNombreDeCase();
            if (listeScoreJoueur.size() > 1) {
                topCouronne();
            }
        }

        //Affiche la liste des vainqueurs
        if (listeScoreJoueur.size() > 1) {
            for (int i = 0; i < listeScoreJoueur.size(); i++) {
                System.out.print(listeJoueur.get(i).getNomDuJoueur() + ", ");
            }
            System.out.println("ont gagné avec : " + score + " points");
        } else {
            System.out.println("C'est " + listeScoreJoueur.get(0).getNomDuJoueur() + " qui a gagné avec " + score + " points");
        }
    }

    /**
     * Remplie listeScoreJoueur avec les gagnants en nombre de points
     */
    private int topScore() {
        int score = -1;
        for (int i = 0; i < nombreDeJoueurs; i++) {
            Joueur joueur = listeJoueur.get(i);
            if (joueur.getScoreJoueur() == score) {
                listeScoreJoueur.add(joueur);
            }
            if (joueur.getScoreJoueur() > score) {
                score = joueur.getScoreJoueur();
                listeScoreJoueur.clear();
                listeScoreJoueur.add(joueur);
            }

        }
        return (score);
    }

    /**
     * Remplie listeScoreJoueur avec les gagnants en nombre de points && nombre de case
     */
    private void topNombreDeCase() {
        int nombreDeCase = 0;
        ArrayList<Joueur> listeScoreJoueurBis = new ArrayList();
        for (int i = 0; i < listeScoreJoueur.size(); i++) {
            Joueur joueur = listeScoreJoueur.get(i);
            if (joueur.getPlateauJoueur().nombreCasesRemplies() == nombreDeCase) {
                listeScoreJoueurBis.add(joueur);
            }
            if (joueur.getPlateauJoueur().nombreCasesRemplies() > nombreDeCase) {
                nombreDeCase = joueur.getPlateauJoueur().nombreCasesRemplies();
                listeScoreJoueurBis.clear();
                listeScoreJoueurBis.add(joueur);
            }
        }
        listeScoreJoueur.clear();
        listeScoreJoueur.addAll(listeScoreJoueurBis);
    }

    /**
     * Remplie listeScoreJoueur avec les gagnants en nombre de points && nombre de case && nombre de couronne
     */
    private void topCouronne() {
        int nombreDeCouronne = 0;
        ArrayList<Joueur> listeScoreJoueurBis = new ArrayList();
        for (int i = 0; i < listeScoreJoueur.size(); i++) {
            Joueur joueur = listeScoreJoueur.get(i);
            if (
                    joueur.getPlateauJoueur().nombreCouronnes() == nombreDeCouronne) {
                listeScoreJoueurBis.add(joueur);
            }
            if (joueur.getPlateauJoueur().nombreCouronnes() > nombreDeCouronne) {
                nombreDeCouronne = joueur.getPlateauJoueur().nombreCouronnes();
                listeScoreJoueurBis.clear();
                listeScoreJoueurBis.add(joueur);
            }
        }
        listeScoreJoueur.clear();
        listeScoreJoueur.addAll(listeScoreJoueurBis);
    }
}